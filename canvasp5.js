import * as THREE from 'three';

import p5 from 'p5';
import { scene } from './main';

export let textureCanvas

let counter = 0
let pts = []
let vels = []

let sketch = function (p) {
    p.setup = function () {
        let canvas = p.createCanvas(1024, 1024);
        canvas.hide()
        p.background(0, 200, 0);

        for (let i = 0; i < 150; i++) {
            pts[i] = p.createVector(p.random(0, p.width), p.random(0, p.height))
            vels[i] = p.createVector(p.random(-2, 2), p.random(-2, 2))
        }

        initThreeJS(); // Llama a la función de inicialización de Three.js aquí
        p.fill(255, 0, 0)
        p.rect(p.width * 0.15, p.height * 0.15, 50, 50);

    };

    p.draw = function () {
        // p.background(0, 255, 0);
        // p.fill(255, 0, 130);
        // counter += 0.1
        // let offx = counter
        // let offy = offx
        // p.translate(p.width * 0.5, p.height * 0.5)
        // p.rotate(p.millis() * 0.005)
        // p.rect(-25 + offx, -25 + offy, 50, 50);

        p.background(0, 30, 0);
        for (let i = 0; i < pts.length; i++) {
            borders(pts[i])
            p.fill(0)
            // p.circle(pts[i].x, pts[i].y, 10)
            for (let k = 0; k < pts.length; k++) {
                if (i != k) {
                    let dist1 = p.dist(pts[i].x, pts[i].y, pts[k].x, pts[k].y)
                    if (dist1 < 110) {
                        p.strokeWeight(1)
                        //stroke(109, 84, 93)
                        //stroke(random(255),random(255),random(255))
                        //stroke((dist1/150)*255)
                        p.stroke(((1 / dist1) * 7000))
                        p.line(pts[i].x, pts[i].y, pts[k].x, pts[k].y)
                    }
                }
            }
            // let dist2 = p.dist(pts[i].x, pts[i].y, mouseX, mouseY)
            // if (dist2 < 150) {
            //     push()
            //     strokeWeight(2)
            //     stroke(105, 105, 225)
            //     line(pts[i].x, pts[i].y, mouseX, mouseY)
            //     pop()
            // }
            pts[i].add(vels[i])
        }

    };

    function borders(pos) {
        if (pos.y > p.height) {
            pos.y = 0
        }
        if (pos.y < 0) {
            pos.y = p.height
        }
        if (pos.x > p.width) {
            pos.x = 0
        }
        if (pos.x < 0) {
            pos.x = p.width
        }
    }
};

export let canvasp5 = new p5(sketch);

// Función para inicializar Three.js
function initThreeJS() {
    // Aquí, puedes estar seguro de que el canvas de p5.js está listo
    // Mueve el código relevante de `main.js` aquí para crear la textura y el plano

    textureCanvas = new THREE.Texture(canvasp5.canvas);
    textureCanvas.needsUpdate = true;


    // let canvasMaterial = new THREE.MeshBasicMaterial({ map: textureCanvas });
    // let geometry = new THREE.PlaneGeometry(2, 2);
    // let plane = new THREE.Mesh(geometry, canvasMaterial);
    // scene.add(plane);

    let canvasMaterial = new THREE.MeshBasicMaterial({ map: textureCanvas });
    let radius = 1; // Define el radio de la esfera
    let widthSegments = 32; // Define la cantidad de segmentos en la dirección horizontal
    let heightSegments = 32; // Define la cantidad de segmentos en la dirección vertical
    let geometry = new THREE.SphereGeometry(radius, widthSegments, heightSegments);

    let sphere = new THREE.Mesh(geometry, canvasMaterial);
    scene.add(sphere);
}
