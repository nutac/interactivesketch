import * as THREE from 'three';

import p5 from 'p5';
import { scene } from './main';

export let textureCanvas

let counter = 0

let sketch = function (p) {
    p.setup = function () {
        p.createCanvas(512, 512);
        p.background(100);
        initThreeJS(); // Llama a la función de inicialización de Three.js aquí
    };

    p.draw = function () {
        // p.background(0, 255, 0);
        p.fill(255, 0, 130);
        counter += 0.1
        let offx = counter
        let offy = offx
        p.translate(p.width * 0.5, p.height * 0.5)
        p.rotate(p.millis() * 0.005)
        // p.rect(p.width * 0.5, p.height * 0.5, 50, 50);
        p.rect(-25 + offx, -25 + offy, 50, 50);

    };
};

export let canvasp5 = new p5(sketch);

// Función para inicializar Three.js
function initThreeJS() {
    // Aquí, puedes estar seguro de que el canvas de p5.js está listo
    // Mueve el código relevante de `main.js` aquí para crear la textura y el plano

    textureCanvas = new THREE.Texture(canvasp5.canvas);
    textureCanvas.needsUpdate = true;
    let canvasMaterial = new THREE.MeshBasicMaterial({ map: textureCanvas });
    let geometry = new THREE.PlaneGeometry(5, 5);
    let plane = new THREE.Mesh(geometry, canvasMaterial);
    scene.add(plane);
}
