import * as THREE from 'three';
import { OrbitControls } from 'three/examples/jsm/controls/OrbitControls.js';

export let camera;

export function createCamera() {
    camera = new THREE.PerspectiveCamera(75, window.innerWidth / window.innerHeight, 0.1, 1000);
    camera.position.z = 3.5;
    // camera.position.x = 2.5
    camera.position.y = -1

    return camera;
}

export function createOrbitControls(_camera, _renderer) {
    return new OrbitControls(_camera, _renderer.domElement);
}

export function moveCamera(camera) {
    camera.position.z += 0.05;
}