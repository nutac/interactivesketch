import * as THREE from 'three';
import { FBXLoader } from 'three/addons/loaders/FBXLoader.js';

// import { canvasMaterial, textureCanvas } from './main';
import { textureCanvas } from './canvasp5';

export let objects = [];
export let objectNames = [
    // 'sphere_1',
    // "sphere_2",
    // 'sphere_3'
];
for (let i = 0; i < 8; i++) {
    let name = 'sphere_' + i
    objectNames.push(name)
}
// objectNames.push('techo')
let objectsTextureNames = ['techo']
let objectsTexture = []



export function loadModel(scene, animate) {
    let loader = new FBXLoader();
    loader.load('sketch_test.fbx', function (object) {
        let box = new THREE.Box3().setFromObject(object);
        let center = box.getCenter(new THREE.Vector3());

        object.position.x += (object.position.x - center.x);
        object.position.y += (object.position.y - center.y);
        object.position.z += (object.position.z - center.z);

        object.traverse(function (child) {
            if (child instanceof THREE.Mesh) {
                objectNames.forEach(name => {
                    // if (child.parent.name.includes(name)) {
                    if (child.parent.name === name) {
                        child.material = new THREE.MeshPhongMaterial({ color: 0x00ffff });
                        child.material.color.setRGB(1, 0, 0)
                        // child.color = { r: 20, g: 200, b: 20 };
                        // console.log(child)
                        child.notingOff = true;
                        child.name = name + '_child';
                        objects.push(child);
                    }
                });

                objectsTextureNames.forEach(name => {
                    if (child.parent.name === name) {
                        // child.material = new THREE.MeshPhongMaterial({ color: 0x00ffff });
                        // child.material.color.setRGB(0, 1, 0)
                        // // child.color = { r: 0, g: 200, b: 0 };
                        // // console.log(child)
                        // child.notingOff = true;
                        // child.name = name + '_child';

                        child.material = new THREE.MeshBasicMaterial({ map: textureCanvas });
                        child.material.color.setHex(0xff0000);
                        // console.log(child.geometry)
                        // if (!child.geometry.faceVertexUvs) {
                        //     child.geometry.faceVertexUvs = [[]];
                        // }
                        // child.geometry.faceVertexUvs[0].push([
                        //     new THREE.Vector2(0, 0),
                        //     new THREE.Vector2(1, 0),
                        //     new THREE.Vector2(1, 1),
                        //     new THREE.Vector2(0, 1)
                        // ]);
                        // child.geometry.uvsNeedUpdate = true;
                        objectsTexture.push(child);
                    }
                });
            }
        });
        console.log(objectsTexture)
        scene.add(object);
        animate();
    });
}

function setColor(object, r, g, b) {
    object.material.color.setRGB(r / 256, g / 256, b / 256)
    // console.log(object.name)
}

let oIndex = 0;
let lastUpdateTime = 0;
const updateInterval = 500;
export function updateColors() {
    const currentTime = Date.now();
    if (currentTime - lastUpdateTime < updateInterval) {
        return
    }
    lastUpdateTime = currentTime;

    oIndex += 1;
    if (oIndex >= objects.length) oIndex = 0;

    // console.log(oIndex, objects[oIndex].name)

    for (let i = 0; i < objects.length; i++) {
        if (oIndex == i) {
            setColor(objects[i], 255, 0, 125);
        } else {
            setColor(objects[i], 10, 10, 10);
        }
    }
}

let lastTextureUpdateTime = 0;
const updateTextureInterval = 500;
export function updateTexture() {
    // const currentTime = Date.now();
    // if (currentTime - lastTextureUpdateTime < updateTextureInterval) {
    //     return
    // }
    // lastTextureUpdateTime = currentTime;

    // for (let i = 0; i < objectsTexture.length; i++) {
    //     let r = parseInt(Math.random() * 256)
    //     let g = parseInt(Math.random() * 256)
    //     let b = parseInt(Math.random() * 256)

    //     setColor(objectsTexture[i], r, g, b);
    // }

    // objectsTexture.forEach(object => {
    //     if (object.material && object.material.map) {
    //         // console.log(object)
    //         object.material.map.needsUpdate = true;
    //         // object.material.needsUpdate = true;

    //     }
    // });

}