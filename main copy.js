import * as THREE from 'three';

import { createScene, createRenderer } from "./scene"
import { createCamera } from './camera'
import { addLights } from '.lights'

init()

/// globals
let scene, camera, renderer, canvas;
let objects;

let objectNames = ['sphere_1', "sphere_2"]

function init() {
  // Crear una escena:
  scene = createScene();

  // Crear una cámara
  camera = createCamera();

  // Crear un renderizador
  renderer = createRenderer();

  /// crear luces:
  addLights(scene)


  // crear funcion y listener para que el canvas se ajuste al windowResize:
  // onWindowREsize=...

  // cargar modelo fbx

  let loader = new FBXLoader();
  loader.load('sketch_test.fbx', function (object) {
    object.traverse(function (child) {
      if (child instanceof THREE.Mesh) {
        objectNames.forEach(name => {
          if (child.parent.name.includes(name)) {
            child.material = new THREE.MeshPhongMaterial({ color: 0x00ffff });
            child.color = { r: 200, g: 20, b: 20 }
            child.notingOff = true;
            child.name = name + '_child'
            objects.push(child);
          }
        });
      }
    });
    scene.add(object);
    animate();
  });

  // Función de animación
  function animate() {
    requestAnimationFrame(animate);
    renderer.render(scene, camera);
  }
}

