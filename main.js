// import * as THREE from 'three';

import { createScene, createRenderer, onWindowResize } from "./scene";
import { createCamera, createOrbitControls, moveCamera } from './camera';
import { addLights } from './lights';
import { loadModel, updateColors, updateTexture } from './model'

import { canvasp5, textureCanvas } from './canvasp5';


export let scene;
export let camera, renderer;
let controls;

// export let textureCanvas
// export let canvasMaterial


init();
function init() {
  scene = createScene();
  camera = createCamera();
  renderer = createRenderer();
  addLights(scene);
  window.addEventListener('resize', onWindowResize, false);
  controls = createOrbitControls(camera, renderer)

  /// canvas
  // textureCanvas = new THREE.Texture(canvasp5.canvas);
  // textureCanvas.needsUpdate = true;
  // canvasMaterial = new THREE.MeshBasicMaterial({ map: textureCanvas });
  // let geometry = new THREE.PlaneGeometry(5, 5);
  // let plane = new THREE.Mesh(geometry, canvasMaterial);
  // scene.add(plane);

  loadModel(scene, animate);
}

function animate() {
  requestAnimationFrame(animate);
  // moveCamera(camera)
  updateColors()
  updateTexture()

  if (textureCanvas) {
    textureCanvas.needsUpdate = true;
  }

  renderer.render(scene, camera);
}
